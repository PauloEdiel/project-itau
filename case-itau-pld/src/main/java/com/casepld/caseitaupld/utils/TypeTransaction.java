package com.casepld.caseitaupld.utils;

public enum TypeTransaction {
    TAKE,
    TRANSFER,
    DEPOSIT
}
