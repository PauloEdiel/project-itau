package com.casepld.caseitaupld.repositories;

import com.casepld.caseitaupld.documents.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TransationRepository extends MongoRepository<Transaction, String> {
}
