package com.casepld.caseitaupld.documents;

import com.casepld.caseitaupld.utils.TypeTransaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class Transaction {

    @Id
    private String id;
    private String cpf;
    private Double value;
    private TypeTransaction typeTransaction;
    private Boolean suspect;

}
