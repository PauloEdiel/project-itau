package com.casepld.caseitaupld.controllers;

import com.casepld.caseitaupld.documents.Transaction;
import com.casepld.caseitaupld.services.RiskAnalysisTransactionService;
import com.casepld.caseitaupld.services.TransationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping("api/v1")
@Slf4j
public class TransactionController {

    @Autowired
    private TransationService transationService;
    @Autowired
    private RiskAnalysisTransactionService analysisTransactionService;


    @GetMapping("/transation")
    public ResponseEntity<List<Transaction>> listAll(){
        return ResponseEntity.ok(this.transationService.listAll());
    }

    @GetMapping("/transation/{id}")
    public ResponseEntity<Optional<Transaction>> findTransaction(@PathVariable("id") String id){
        return ResponseEntity.ok(this.transationService.listById(id));
    }

    @PostMapping("/new-transaction")
    public ResponseEntity messageToTopic(@RequestBody Transaction transaction){
        analysisTransactionService.analysis(transaction);
        log.info("New {} ",transaction.toString());
        return ResponseEntity.ok("Transaction Published Successfully");
    }
}
