package com.casepld.caseitaupld.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ConsumerService{


    @KafkaListener(topics = "transactions_topic", groupId = "transactions")

    public void consumerMessage(String message) {
        log.info("Transaction Suspect Detected -> {} ", message.toString());
    }
}
