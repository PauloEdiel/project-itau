package com.casepld.caseitaupld.services;

import com.casepld.caseitaupld.documents.Transaction;
import com.casepld.caseitaupld.repositories.TransationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TransationService {

    @Autowired
    private TransationRepository transationRepository;

    public List<Transaction> listAll() {
        return this.transationRepository.findAll();
    }

    public Optional<Transaction> listById(String id) {
        return this.transationRepository.findById(id);
    }

    public Transaction create(Transaction transaction) {
        return this.transationRepository.save(transaction);
    }

    public Transaction update(Transaction transaction) {
        return this.transationRepository.save(transaction);
    }
}
