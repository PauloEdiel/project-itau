package com.casepld.caseitaupld.services;

import com.casepld.caseitaupld.documents.Transaction;
import com.casepld.caseitaupld.repositories.TransationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiskAnalysisTransactionService {

    @Autowired
    private final ProducerService producer;
    @Autowired
    private final TransationRepository transationRepository;

    public RiskAnalysisTransactionService(ProducerService producerService, TransationRepository repository) {
        this.producer = producerService;
        this.transationRepository = repository;
    }

    public void analysis(Transaction transaction) {

        if (transaction.getValue() > 2000) {
            transaction.setSuspect(true);
            this.producer.sendMessage(transaction.toString());
        }
        this.transationRepository.save(transaction);
    }
}
