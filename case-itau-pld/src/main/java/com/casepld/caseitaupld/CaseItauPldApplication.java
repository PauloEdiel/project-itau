package com.casepld.caseitaupld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaseItauPldApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaseItauPldApplication.class, args);
	}

}
